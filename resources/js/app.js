const Cookies = require('js-cookie');

if (Cookies.get('dark-mode') !== 'false') {
  document.documentElement.classList.add('mode-dark');
}

document.getElementById('toggle-mode').addEventListener('click', function (event) {
  event.preventDefault();
  Cookies.set('dark-mode', ! document.documentElement.classList.contains('mode-dark'));
  document.documentElement.classList.toggle('mode-dark');
});