<nav class="text-gray-900 dark:text-gray-100 transition-color fixed top-0 inset-x-0">
  <ul class="flex justify-end px-10 py-4 items-center -mx-2">
    <li>
      <a href="{{ route('home') }}" class="font-bold px-2 block">Home</a>
    </li>
    <li class="ml-4">
      <a href="#" id="toggle-mode" class="font-bold p-2 block text-gray-100 bg-gray-900 rounded text-sm flex items-center transition-colors group">
        <svg class="fill-current inline-block h-4 text-gray-500 group-hover:text-yellow-400 dark:text-yellow-400 align-middle transition-color" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M283.211 512c78.962 0 151.079-35.925 198.857-94.792 7.068-8.708-.639-21.43-11.562-19.35-124.203 23.654-238.262-71.576-238.262-196.954 0-72.222 38.662-138.635 101.498-174.394 9.686-5.512 7.25-20.197-3.756-22.23A258.156 258.156 0 0 0 283.211 0c-141.309 0-256 114.511-256 256 0 141.309 114.511 256 256 256z"/></svg>
        <span class="ml-2">Dark Mode</span>
      </a>
    </li>
  </ul>
</nav>