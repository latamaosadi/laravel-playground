<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Laravel Playground</title>

  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Nunito:400,700,800,900&display=swap" rel="stylesheet">

  <!-- Styles -->
  <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
</head>
<body class="font-sans">
  <div class="min-h-screen bg-gray-100 dark:bg-gray-900 transition-colors">
    @include('includes.navbar')
    @yield('content')
    @include('includes.footer')
  </div>

  <script src="{{ mix('/js/app.js') }}" type="text/javascript"></script>
</body>
</html>