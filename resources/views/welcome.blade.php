@extends('layouts.app')

@section('content')

<div class="min-h-screen flex">
    <div class="m-auto text-center text-gray-900 dark:text-gray-100 transition-color">
        <h1 class="font-normal text-5xl">
            <span class="text-red-600 dark:text-red-500 font-bold">Laravel</span>Playground
        </h1>
        <p class="mt-4 text-gray-400 dark:text-gray-700">
            Welcome to laravel playground
        </p>
    </div>
</div>

@endsection