# Laravel Playground

This is a personal sandbox to play around with laravel. Each experiment will be segmented as it's own branch.

Feel free to browse the branches of the repository.